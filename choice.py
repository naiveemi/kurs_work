from sort_lib import quicksort, shell_sort, heap_sort, timsort

lst = list(map(int, input('Введите числа через пробел: ').split()))
choice_sort = int(input('Выберите сортировку:\nбыстрая сортировка - 1'
                        '\nсортировка шелла - 2\nпирамидальная сортировка - 3\nтимсорт - 4\n'))
choice_reverse = int(input('По убыванию - 1, по возрастанию - 2\n'))
if choice_sort == 1 and choice_reverse == 1:
    print(quicksort(lst))
elif choice_sort == 1 and choice_reverse == 2:
    print(quicksort(lst, reverse=True))
elif choice_sort == 2 and choice_reverse == 1:
    print(shell_sort(lst))
elif choice_sort == 2 and choice_reverse == 2:
    print(shell_sort(lst, reverse=True))
elif choice_sort == 3 and choice_reverse == 1:
    print(heap_sort(lst))
elif choice_sort == 3 and choice_reverse == 2:
    print(heap_sort(lst, reverse=True))
elif choice_sort == 4 and choice_reverse == 1:
    print(timsort(lst))
elif choice_sort == 4 and choice_reverse == 2:
    print(timsort(lst, reverse=True))
