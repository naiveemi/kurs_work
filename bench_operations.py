import cProfile
import pstats
from pstats import SortKey
import matplotlib.pyplot as plt
from random import randint
from sort_lib import quicksort, shell_sort, heap_sort, timsort


def correct_lines_c(f):
    result = 1
    lines = [line.strip() for line in f]
    lines_1 = [line for line in lines if line not in '' and line.split()[0].isnumeric() and not line.count('function')]
    for line in lines_1:
        if line.find('') != -1:
            result += int(line.split('   ')[0])
            break
    return result


operation_count = []
operation_lst = []
step = 100
max_size = 10000
min_size = 1000

for j in range(step, max_size, min_size):
    lst = []
    for x in range(j):
        lst.append(randint(-100, 100))
    operation_count.append(len(lst))
    cProfile.run('quicksort(lst)', 'stats.log')
    with open('output.txt', 'w') as log_file_stream:
        p = pstats.Stats('stats.log', stream=log_file_stream)
        p.strip_dirs().sort_stats(SortKey.CALLS).print_stats()
    f = open('output.txt')
    line = correct_lines_c(f)
    f.close()
    operation_lst.append(int(line))
operation_lst_c = [i for i in operation_lst]

plt.style.use('seaborn-whitegrid')
fig, ax = plt.subplots()

ax.set_title('Зависимость количества операций от длины массива', fontsize=14, c='indigo')
ax.set_xlabel('Размер массива', fontsize=14, c='indigo')
ax.set_ylabel('Количество операций', fontsize=14, c='indigo')

ax.plot(operation_count, operation_lst_c, lw=2, c='g', label='Быстрая сортировка')

plt.legend()
plt.show()