from random import randint
import matplotlib.pyplot as plt
from sort_lib import quicksort, shell_sort, heap_sort, timsort
from timeit import default_timer
import sys

sys.setrecursionlimit(5000)


def spent_time(function, array: list, repeats=1):
    result = 0
    for i in range(repeats):
        start_clock = default_timer()  # default_timer даёт время выполнения в секундах
        function(array)
        end_clock = default_timer() - start_clock

        result += end_clock
    return result/repeats


step = 100
max_size = 5000
min_size = 1000

names = [[], [], [], []]
names_count = []


for j in range(step, max_size + step, min_size):
    arr = [randint(-100, 100) for x in range(j)]
    names_count.append(len(arr))
    names[0].append(spent_time(quicksort, arr))
    names[1].append(spent_time(shell_sort, arr))
    names[2].append(spent_time(heap_sort, arr))
    names[3].append(spent_time(timsort, arr))


plt.style.use('seaborn-whitegrid')
fig, ax = plt.subplots()


ax.set_title('', fontsize=20, c='indigo')
ax.set_xlabel('Размер массива', fontsize=14, c='indigo')
ax.set_ylabel('Время выполнения', fontsize=14, c='indigo')

ax.plot(names_count, names[0], lw=2, c='crimson', label='quicksort')
ax.plot(names_count, names[1], lw=2, c='g', label='shell_sort')
ax.plot(names_count, names[2], lw=2, c='darkblue', label='heap_sort')
ax.plot(names_count, names[3], lw=2, c='deepskyblue', label='timsort')

plt.legend()
plt.show()

