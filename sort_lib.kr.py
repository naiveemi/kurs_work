# quicksort
def quicksort(lst, reverse=False):  # задаётся функция
    if len(lst) <= 1:
        return lst
    elem = lst[0]  # выбирается опорный элемент
    left = list(filter(lambda x: x < elem, lst))
    center = list(filter(lambda x: x == elem, lst))
    right = list(filter(lambda x: x > elem, lst))
    if not reverse:
        return quicksort(left) + center + quicksort(right)
    elif reverse:
        return list(reversed(quicksort(left) + center + quicksort(right)))


# shell's sort
def shell_sort(lst, reverse=False):
    cut_res = len(lst) // 2
    while cut_res > 0:
        for value in range(cut_res, len(lst)):
            current_value = lst[value]
            position = value
            while position >= cut_res and lst[position - cut_res] > current_value:
                lst[position] = lst[position - cut_res]
                position -= cut_res
                lst[position] = current_value
        cut_res //= 2
    if not reverse:
        return lst
    elif reverse:
        return list(reversed(lst))


# heap_sort
def heapify(lst, heap_size, root_index):
    largest = root_index
    left_child = (2 * root_index) + 1
    right_child = (2 * root_index) + 2
    if left_child < heap_size and lst[left_child] > lst[largest]:
        largest = left_child
    if right_child < heap_size and lst[right_child] > lst[largest]:
        largest = right_child
    if largest != root_index:
        lst[root_index], lst[largest] = lst[largest], lst[root_index]
        heapify(lst, heap_size, largest)
        return lst


def heap_sort(lst, reverse=False):
    n = len(lst)
    for i in range(n, -1, -1):
        heapify(lst, n, i)
    for i in range(n - 1, 0, -1):
        lst[i], lst[0] = lst[0], lst[i]
        heapify(lst, i, 0)
    if not reverse:
        return lst
    elif reverse:
        return list(reversed(lst))


# timsort
min_run = 32


def insertion_sort(lst, start, end):
    for i in range(start + 1, end + 1):
        elem = lst[i]
        j = i - 1
        while j >= start and elem < lst[j]:
            lst[j + 1] = lst[j]
            j -= 1
        lst[j + 1] = elem
    return lst


def merge(lst, start, mid, end):
    if mid == end:
        return lst
    first = lst[start:mid + 1]
    last = lst[mid + 1:end + 1]
    len1 = mid - start + 1
    len2 = end - mid
    ind1 = 0
    ind2 = 0
    ind = start

    while ind1 < len1 and ind2 < len2:
        if first[ind1] < last[ind2]:
            lst[ind] = first[ind1]
            ind1 += 1
        else:
            lst[ind] = last[ind2]
            ind2 += 1
        ind += 1

    while ind1 < len1:
        lst[ind] = first[ind1]
        ind1 += 1
        ind += 1

    while ind2 < len2:
        lst[ind] = last[ind2]
        ind2 += 1
        ind += 1

    return lst


def timsort(lst, reverse=False):
    n = len(lst)

    for start in range(0, n, min_run):
        end = min(start + min_run - 1, n - 1)
        lst = insertion_sort(lst, start, end)

    curr_size = min_run
    while curr_size < n:
        for start in range(0, n, curr_size * 2):
            mid = min(n - 1, start + curr_size - 1)
            end = min(n - 1, mid + curr_size)
            lst = merge(lst, start, mid, end)
        curr_size *= 2
    if not reverse:
        return lst
    elif reverse:
        return list(reversed(lst))
