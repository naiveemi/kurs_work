import pytest
from random import randint
from sort_lib import quicksort, shell_sort, heap_sort, timsort


def test_ascending():
    lst = []
    for i in range(20):
        lst.append(randint(-100, 100))
    theory = sorted(lst)
    practice_1 = quicksort(lst)
    assert theory == list(practice_1)
    practice_2 = shell_sort(lst)
    assert theory == list(practice_2)
    practice_3 = heap_sort(lst)
    assert theory == list(practice_3)
    practice_4 = timsort(lst)
    assert theory == list(practice_4)


def test_descending():
    lst = []
    for i in range(20):
        lst.append(randint(-100, 100))
    theory = list(reversed(sorted(lst)))
    practice_1 = quicksort(lst, reverse=True)
    assert theory == list(practice_1)
    practice_2 = shell_sort(lst, reverse=True)
    assert theory == list(practice_2)
    practice_3 = heap_sort(lst, reverse=True)
    assert theory == list(practice_3)
    practice_4 = timsort(lst, reverse=True)
    assert theory == list(practice_4)


def test_input_data():
    lst = [1, 2, 3.41125, '3.14', 'abc']
    with pytest.raises(TypeError):
        list(quicksort(lst))
        list(shell_sort(lst))
        list(heap_sort(lst))
        list(timsort(lst))


def test_ascending_stability():
    lst = [(1, 1), (2, 5), (3, 2), (4, 6), (5, 5), (6, 10), (7, 8), (8, 5)]
    theory = sorted(lst)
    practice_1 = quicksort(lst)
    assert theory == list(practice_1)
    practice_2 = shell_sort(lst)
    assert theory == list(practice_2)
    practice_3 = heap_sort(lst)
    assert theory == list(practice_3)
    practice_4 = timsort(lst)
    assert theory == list(practice_4)


def test_descending_stability():
    lst = [(1, 1), (2, 5), (3, 2), (4, 6), (5, 5), (6, 10), (7, 8), (8, 5)]
    theory = list(reversed(sorted(lst)))
    practice_1 = quicksort(lst, reverse=True)
    assert theory == list(practice_1)
    practice_2 = shell_sort(lst, reverse=True)
    assert theory == list(practice_2)
    practice_3 = heap_sort(lst, reverse=True)
    assert theory == list(practice_3)
    practice_4 = timsort(lst, reverse=True)
    assert theory == list(practice_4)
